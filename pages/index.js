import React from 'react'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import VideoPlayer from '../components/VideoPlayer'

const styles = theme => ({
  root: {
    textAlign: 'center',
    padding: theme.spacing.unit * 3
  }
})

class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    const { classes } = this.props

    return (
      <div className={classes.root}>
        <Typography variant="display1" align="left" gutterBottom>My Video Player</Typography>
        <VideoPlayer videoSrc="/static/sintel_trailer-480p.mp4" videoName="Sintel Trailer" />
      </div>
    )
  }
}

Index.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(Index)