import React from 'react'
import Thumbnail from '../components/Thumbnail'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import shortid from 'shortid'
import VideoComponent from '../components/VideoComponent'
import Grid from '@material-ui/core/Grid'
import SearchField from '../components/SearchField'

const styles = () => ({
  container: {
    flexGrow: 1
  },
  overflow: {
    maxHeight: '600px',
    overflowY: 'auto'
  },
  searchcontainer: {
    textAlign: 'left'
  }
})

class VideoPlayer extends React.Component {

  constructor(props) {
    super(props)
    const originalPlaylist = [
      {
        id: 'fullvideo',
        start: '00:00',
        name: this.props.videoName ? this.props.videoName : '',
        end: null,
        playing: true,
        tags: []
      },
      {
        id: shortid.generate(),
        name: 'Clip 1',
        start: '00:10',
        end: '00:15',
        playing: false,
        tags: ['first', 'sintel', 'jump']
      },
      {
        id: shortid.generate(),
        name: 'Clip 2',
        start: '00:20',
        end: '00:25',
        playing: false,
        tags: ['second', 'sintel', 'trailer']
      },
      {
        id: shortid.generate(),
        name: 'Clip 3',
        start: '00:30',
        end: '00:35',
        playing: false,
        tags: ['third', 'jump']
      }
    ]
    this.state = {
      originalVideo: this.props.videoSrc ? this.props.videoSrc : '',
      videoSrc: this.props.videoSrc ? this.props.videoSrc : '',
      videoName: this.props.videoName ? this.props.videoName : '',
      player: null,
      timer: null,
      loading: false,
      playlist: originalPlaylist,
      filteredPlaylist: originalPlaylist
    }
  }

  componentDidMount() {
    document.addEventListener('keydown', this.keyListener)
  }
  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyListener)
  }

  keyListener = e => {
    if (e.keyCode === 37) {
      this.playPrevClip()
    } else if (e.keyCode === 39) {
      this.playNextClip()
    }
  }

  updateAndPlayVideo = clip => {

    if (this.state.timer != null) {
      clearTimeout(this.state.timer)
      this.setState({
        loading: false,
        timer: null
      })
    }

    const newPlaylist = this.state.filteredPlaylist.map(item => {
      if (item.id === clip.id) {
        return { ...clip, playing: true }
      }
      return { ...item, playing: false }
    })

    const src = this.state.originalVideo + '#t=' + clip.start + (clip.end ? ',' + clip.end : '')
    const player = this.state.player
    this.setState({
      filteredPlaylist: newPlaylist,
      videoSrc: src,
      videoName: clip.name
    })
    player.load()
    player.play()
  }

  handleSwitchVideo = clip => {
    this.updateAndPlayVideo(clip)
  }

  handleAddNewClip = clip => {
    clip.tags = clip.tags.split(',').map(item => item.trim())
    const newplaylist = [...this.state.playlist, { ...clip, id: shortid.generate(), playing: false }]
    this.setState({
      playlist: newplaylist,
      filteredPlaylist: newplaylist
    })
  }

  handleEditClip = clip => {
    clip.tags = clip.tags.split(',').map(item => item.trim())
    const newplaylist = this.state.playlist.map(item => {
      if (item.id === clip.id) {
        return clip
      }
      return item
    })
    this.setState({
      playlist: newplaylist,
      filteredPlaylist: newplaylist
    })

    const current = this.state.filteredPlaylist.filter(item => item.playing === true)[0]
    if (current.id === clip.id) {
      this.updateAndPlayVideo(clip)
    }
  }

  handleRemoveClip = clip => {
    const current = this.state.playlist.filter(item => item.playing === true)[0]

    const newplaylist = this.state.playlist.filter(item => item.id !== clip.id)
    this.setState({
      playlist: newplaylist,
      filteredPlaylist: newplaylist
    }, () => {
      if (current.id === clip.id) {
        this.updateAndPlayVideo(this.state.filteredPlaylist[0])
      }
    })
  }

  handleFilterPlaylist = filter => {
    if (filter !== '') {
      const newPlaylist = this.state.playlist.filter(item => item.tags.indexOf(filter) !== -1)
      this.setState({
        filteredPlaylist: newPlaylist
      }, () => {
        if (newPlaylist.length > 0) {
          this.updateAndPlayVideo(this.state.filteredPlaylist[0])
        }
      })
    } else {
      this.setState({
        filteredPlaylist: this.state.playlist
      }, () => {
        this.updateAndPlayVideo(this.state.filteredPlaylist[0])
      })
    }
  }

  getRefsFromChild = childRefs => {
    const player = childRefs.player
    const firstClip = this.state.playlist[0]
    const newplaylist = this.state.playlist.map(item => {
      if (item.id === firstClip.id) {
        const minutes = Math.trunc(player.duration / 60)
        const seconds = Math.trunc(player.duration % 60)
        const duration = minutes + ':' + seconds
        return { ...item, end: duration }
      }
      return item
    })
    this.setState({
      player: player,
      playlist: newplaylist,
      filteredPlaylist: newplaylist
    }, () => {
      player.onpause = () => {
        this.scheduleNextClip()
      }
    })
  }

  scheduleNextClip = () => {
    const current = this.state.filteredPlaylist.findIndex(item => item.playing === true)
    if (current < this.state.filteredPlaylist.length - 1) {
      const clip = this.state.filteredPlaylist[current]
      const split = clip.end.split(':')
      const seconds = (split[0] * 60 + split[1]) * 1

      const player = this.state.player
      if (Math.trunc(player.currentTime) === seconds) {
        this.setState({
          loading: true,
          timer: setTimeout(() => {
            this.updateAndPlayVideo(this.state.filteredPlaylist[current + 1])
          }, 3000)
        })
      }
    }
  }

  playPrevClip = () => {
    const current = this.state.filteredPlaylist.findIndex(item => item.playing === true)
    if (current > 0) {
      this.updateAndPlayVideo(this.state.filteredPlaylist[current - 1])
    }
  }

  playNextClip = () => {
    const current = this.state.filteredPlaylist.findIndex(item => item.playing === true)
    if (current < this.state.filteredPlaylist.length - 1) {
      this.updateAndPlayVideo(this.state.filteredPlaylist[current + 1])
    }
  }

  render() {

    const { classes } = this.props

    return (
      <Grid container className={classes.container} spacing={16}>
        <Grid item sm={12} md={8}>
          <VideoComponent
            videoSrc={this.state.videoSrc}
            videoName={this.state.videoName}
            loading={this.state.loading}
            passRefUpward={this.getRefsFromChild} />
        </Grid>
        <Grid item sm={12} md={4}>
          <div className={classes.searchcontainer}>
            <SearchField handleClickOk={this.handleFilterPlaylist} />
          </div>
          <br />
          <div className={classes.overflow}>
            <div>
              {this.state.filteredPlaylist.map(e => {
                if (e.id === 'fullvideo') {
                  return (<Thumbnail key={shortid.generate()}
                    fragment={this.state.filteredPlaylist[0]}
                    handleClick={this.handleSwitchVideo}
                    handleClickEdit={this.handleAddNewClip}
                  />
                  )
                } else {
                  return (
                    <Thumbnail
                      fragment={e}
                      controls={true}
                      handleClick={this.handleSwitchVideo}
                      handleClickEdit={this.handleEditClip}
                      handleClickDelete={this.handleRemoveClip}
                      key={shortid.generate()} />
                  )
                }
              })}
            </div>
          </div>
        </Grid>
      </Grid>
    )
  }
}

VideoPlayer.propTypes = {
  classes: PropTypes.object.isRequired,
  videoSrc: PropTypes.string.isRequired,
  videoName: PropTypes.string.isRequired
}

export default withStyles(styles)(VideoPlayer)