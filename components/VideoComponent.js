import React from 'react'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

const styles = () => ({
  video: {
    width: '100%',
    height: '100%',
    minWidth: '350px'
  },
  loading: {
    width: '200px',
    height: '150px',
    backgroundColor: 'grey',
    zIndex: '100',
    position: 'absolute',
    top: '240px',
    left: '350px'
  }
})

class VideoComponent extends React.Component {

  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.passRefUpward(this.refs)
  }

  render() {

    const { classes, loading, videoSrc, videoName } = this.props

    return (
      <div>
        {loading ? <div className={classes.loading}>Autoplay in 3 sec...</div> : <div></div>}
        <video controls={loading ? false : true} ref="player" className={classes.video} autoPlay>
          <source src={videoSrc} type="video/mp4" />
          Your browser does not support HTML5 video
        </video>
        <br /><br />
        <Typography variant="title" align="left">{videoName}</Typography>
      </div>
    )
  }
}

VideoComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  videoSrc: PropTypes.string.isRequired,
  videoName: PropTypes.string.isRequired,
  passRefUpward: PropTypes.func.isRequired
}

export default withStyles(styles)(VideoComponent)
