HTML Video Player

Made with React, Next.js and Material-UI

The command `npm run dev` executes the project in development mode, go to http://localhost:3000  
The command `npm run build` compiles the production build   
The command `npm run start` executes the project in production mode, go to http://localhost:3000  

Working with:  
node 8.11.3  
npm 5.6.0