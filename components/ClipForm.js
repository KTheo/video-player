import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

const styles = theme => ({
  input: {
    marginBottom: theme.spacing.unit * 2
  }
})

class NewClip extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      open: false,
      clipid: this.props.id ? this.props.id : '',
      clipname: this.props.name ? this.props.name : '',
      clipstart: this.props.start ? this.props.start : '',
      clipend: this.props.end ? this.props.end : '',
      cliptags: this.props.tags ? this.props.tags.join(',') : ''
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    })
  }

  handleClickOk = () => {
    //TODO validate inputs
    this.props.handleClickOk({
      id: this.state.clipid,
      name: this.state.clipname,
      start: this.state.clipstart,
      end: this.state.clipend,
      tags: this.state.cliptags,
    })
    this.setState({
      open: false,
      clipname: '',
      clipstart: '',
      clipend: '',
      cliptags: ''
    })
  }

  render() {

    const { classes } = this.props
    const { clipname, clipstart, clipend, cliptags } = this.state

    return (
      <React.Fragment>
        <Button {...this.props.buttonStyle} onClick={this.handleClickOpen}>{this.props.buttonName}</Button>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            <DialogContentText>
              Please specify the name, start time and end time for the clip
            </DialogContentText>
            <TextField
              className={classes.input}
              label="Name"
              fullWidth
              value={clipname}
              onChange={this.handleChange('clipname')}
            />
            <TextField
              className={classes.input}
              label="Start Time"
              fullWidth
              placeholder="00:00"
              value={clipstart}
              onChange={this.handleChange('clipstart')}
            />
            <TextField
              className={classes.input}
              label="End Time"
              fullWidth
              placeholder="00:00"
              value={clipend}
              onChange={this.handleChange('clipend')}
            />
            <TextField
              className={classes.input}
              label="Tags (Separated by comma)"
              fullWidth
              value={cliptags}
              onChange={this.handleChange('cliptags')}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleClickOk} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    )
  }
}

NewClip.propTypes = {
  buttonName: PropTypes.string.isRequired,
  buttonStyle: PropTypes.object,
  id: PropTypes.string,
  name: PropTypes.string,
  start: PropTypes.string,
  end: PropTypes.string,
  tags: PropTypes.array,
  classes: PropTypes.object.isRequired,
  handleClickOk: PropTypes.func.isRequired
}

export default withStyles(styles)(NewClip)
