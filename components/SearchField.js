import React from 'react'
import TextField from '@material-ui/core/TextField'
import { withStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'

const styles = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center'
  },
  overflow: {
    maxHeight: '600px',
    overflowY: 'auto'
  },


  bootstrapRoot: {
    padding: 0,
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  bootstrapInput: {
    borderRadius: 4,
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 12px',
    width: 'calc(100% - 24px)',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
  bootstrapFormLabel: {
    fontSize: 18,
  },
})

class SearchField extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      value: ''
    }
  }

  handleChange = event => {
    this.setState({
      value: event.target.value
    })
  }

  render() {

    const { classes } = this.props

    return (
      <div className={classes.container}>
        <TextField
          label={null}
          id="search-tags"
          value={this.state.value}
          onChange={this.handleChange}
          type="search"
          placeholder="Search tags"
          InputProps={{
            disableUnderline: true,
            classes: {
              root: classes.bootstrapRoot,
              input: classes.bootstrapInput,
            }
          }}
        />
        <Button variant="outlined" size="large" onClick={() => this.props.handleClickOk(this.state.value)}>
          <span>Ok</span>
        </Button>
      </div>
    )
  }

}

SearchField.propTypes = {
  classes: PropTypes.object.isRequired,
  handleClickOk: PropTypes.func.isRequired
}


export default withStyles(styles)(SearchField)