import React from 'react'
import PropTypes from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import ClipForm from '../components/ClipForm'
import Typography from '@material-ui/core/Typography'
import shortid from 'shortid'

const styles = theme => ({
  container: {
    display: 'table',
    backgroundColor: 'whitesmoke',
    marginBottom: theme.spacing.unit * 2,
    width: '350px',
    maxWidth: '350px',
    minHeight: '110px'
  },
  dark: {
    backgroundColor: '#CFD8DC',
  },
  thumbnailcontainer: {
    display: 'table-cell',
    width: '180px',
    maxWidth: '180px',
    verticalAlign: 'middle'
  },
  informationcontainer: {
    display: 'table-cell',
    textAlign: 'left',
    verticalAlign: 'top'
  },
  button: {
    margin: 0,
    textAlign: 'left',
    paddingLeft: '4px',
    paddingBottom: '4px',
    minWidth: theme.spacing.unit * 5,
    minHeight: '32px'
  },
  timelabel: {
    paddingLeft: '4px'
  },
  tagscontainer: {
    paddingLeft: '4px',
    color: '#2196F3',
    wordBreak: 'break-all'
  }
})

class Thumbnail extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    const { classes, fragment, handleClick, handleClickEdit, handleClickDelete, controls } = this.props

    const containerStyle = [classes.container]
    if (fragment.playing === true) {
      containerStyle.push(classes.dark)
    }

    return (
      <div className={containerStyle.join(' ')}>
        <div className={classes.thumbnailcontainer}>
          <Button color="primary" onClick={() => handleClick(fragment)}>
            <img src="/static/placeholder.png" width={150} />
          </Button>
        </div>
        <div className={classes.informationcontainer}>
          <Button color="primary" className={classes.button} variant="flat" onClick={() => handleClick(fragment)}>
            <span>{fragment.name}</span>
          </Button>
          <Typography
            variant="body1"
            align="left"
            className={classes.timelabel}>
            {fragment.start + ' - ' + (fragment.end ? fragment.end : '')}
          </Typography>
          {
            controls ?
              <div>
                <ClipForm
                  handleClickOk={handleClickEdit}
                  buttonName="Edit"
                  buttonStyle={{ color: 'secondary', size: 'small', className: classes.button }}
                  id={fragment.id}
                  name={fragment.name}
                  start={fragment.start}
                  end={fragment.end}
                  tags={fragment.tags}
                />
                <Button color="secondary" className={classes.button} size="small" onClick={() => handleClickDelete(fragment)}>
                  <span>Delete</span>
                </Button>
              </div>
              :
              <div>
                <ClipForm
                  handleClickOk={handleClickEdit}
                  buttonName="Clip this video"
                  buttonStyle={{ color: 'secondary', className: classes.button }}
                />
              </div>
          }
          <div className={classes.tagscontainer}>
            {fragment.tags.map(tag => {
              return (<small key={shortid.generate()}>{tag}&nbsp;&nbsp;</small>)
            })}
          </div>
        </div>
      </div>
    )
  }
}

Thumbnail.propTypes = {
  fragment: PropTypes.object.isRequired,
  controls: PropTypes.bool,
  classes: PropTypes.object.isRequired,
  handleClick: PropTypes.func.isRequired,
  handleClickEdit: PropTypes.func,
  handleClickDelete: PropTypes.func
}

export default withStyles(styles)(Thumbnail)